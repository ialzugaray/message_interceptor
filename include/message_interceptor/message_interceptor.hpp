#include <ros/ros.h>
#include <boost/shared_ptr.hpp>
#ifndef MESSAGE_INTERCEPTOR_HPP_
#define MESSAGE_INTERCEPTOR_HPP_

#define MESSAGE_TYPE nav_msgs::Odometry
#include <nav_msgs/Odometry.h>

namespace msgInterceptor {

class MessageInterceptor {
public:
  MessageInterceptor();
  MessageInterceptor(std::string input_topic, std::string output_topic);
  void setInputTopic(std::string topic);
  void setOutputTopic(std::string topic);
  std::string getInputTopic();
  std::string getOutputTopic();

protected:
  ros::NodeHandle nh_;
  std::string input_topic_;
  std::string output_topic_;
  ros::Subscriber subscriber_;
  ros::Publisher publisher_;
  void interceptionCallback(const MESSAGE_TYPE::ConstPtr& msg);
  void operation(const MESSAGE_TYPE *input_msg, MESSAGE_TYPE *output_msg);
};
}

namespace msgInterceptor {

MessageInterceptor::MessageInterceptor(){}

MessageInterceptor::MessageInterceptor(std::string input_topic,
                                                    std::string output_topic) {
  ROS_INFO("Constructor");
  nh_ = ros::NodeHandle();
  this->setInputTopic(input_topic);
  this->setOutputTopic(output_topic);
}

void MessageInterceptor::setInputTopic(const std::string topic) {
  ROS_INFO("setInputTopic");
  input_topic_ = topic;
  subscriber_ = nh_.subscribe(input_topic_, 1,
        &MessageInterceptor::interceptionCallback, this);
}

void MessageInterceptor::setOutputTopic(const std::string topic) {
  ROS_INFO("setOutputTopic");
  output_topic_= topic;
  publisher_ = nh_.advertise<MESSAGE_TYPE>(output_topic_, 1);
}

std::string MessageInterceptor::getInputTopic(){ return input_topic_;}

std::string MessageInterceptor::getOutputTopic(){return output_topic_;}

void MessageInterceptor::interceptionCallback(const MESSAGE_TYPE::ConstPtr& msg) {
  ROS_INFO("InterceptionCallback");
  MESSAGE_TYPE* msg_processed;
  operation(msg.get(), msg_processed);
  ROS_INFO("Sending odometry");
  publisher_.publish(*msg_processed);
  ROS_INFO("Sent odometry");
}

void MessageInterceptor::operation(const MESSAGE_TYPE *input_msg,
                                   MESSAGE_TYPE *output_msg){
  ROS_INFO("Operation");
  *output_msg = *input_msg;
  ROS_INFO("Performed empty operation");
}

}


#endif // MESSAGE_INTERCEPTOR_HPP_
