#include <ros/ros.h>
#include <boost/shared_ptr.hpp>
#ifndef MESSAGE_INTERCEPTOR_HPP_
#define MESSAGE_INTERCEPTOR_HPP_


namespace msgInterceptor {
template <typename messageType>
class MessageInterceptor {
public:
  MessageInterceptor();
  MessageInterceptor(std::string input_topic, std::string output_topic);
  void setInputTopic(std::string topic);
  void setOutputTopic(std::string topic);
  std::string getInputTopic();
  std::string getOutputTopic();

protected:
  ros::NodeHandle nh_;
  std::string input_topic_;
  std::string output_topic_;
  ros::Subscriber subscriber_;
  ros::Publisher publisher_;
  void interceptionCallback(boost::shared_ptr<messageType> const& msg);
  void operation(messageType *input_msg, messageType *output_msg);
};
}

namespace msgInterceptor {

template <typename messageType>
MessageInterceptor<messageType>::MessageInterceptor(){}


template <typename messageType>
MessageInterceptor<messageType>::MessageInterceptor(std::string input_topic,
                                                    std::string output_topic) {
  nh_ = ros::NodeHandle();

  this->setInputTopic(input_topic);
  this->setOutputTopic(output_topic);

}

template <typename messageType>
void MessageInterceptor<messageType>::setInputTopic(const std::string topic) {
  input_topic_ = topic;
  subscriber_ = nh_.subscribe(input_topic_, 1,
        &MessageInterceptor<messageType>::interceptionCallback, this);
}

template <typename messageType>
void MessageInterceptor<messageType>::setOutputTopic(const std::string topic) {
  output_topic_= topic;
  publisher_ = nh_.advertise<messageType>(output_topic_, 1);
}

template <typename messageType>
std::string MessageInterceptor<messageType>::getInputTopic(){
  return input_topic_;
}

template <typename messageType>
std::string MessageInterceptor<messageType>::getOutputTopic(){
  return output_topic_;
}

template <typename messageType>
void MessageInterceptor<messageType>::interceptionCallback(
    boost::shared_ptr<messageType> const& msg) {
  messageType* msg_processed;
  operation(msg, msg_processed);

  publisher_.publish(*msg_processed);
}

}


#endif // MESSAGE_INTERCEPTOR_HPP_
