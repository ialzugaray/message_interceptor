#include <ros/ros.h>

#include <sstream>
//#include <nav_msgs/Odometry.h>
#include <std_msgs/Empty.h>

#include <message_interceptor/message_interceptor.hpp>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "message_interceptor_node");
  ROS_INFO("Starting node");
  std::string input_topic = "input";
  std::string output_topic = "output";
  msgInterceptor::MessageInterceptor test_obj(input_topic, output_topic);
  ROS_INFO("Spinning");

/*
  if (argc!=5) {
    ROS_FATAL("Incorrect number of arguments.\n\
usage: uncertain_pointcloud_filter_node \
<input_topic> <output_topic> \
<uncertain_sensor_tf_frame> <groundtruth_sensor_tf_frame>");
    return -1;
  }



  ROS_INFO("Uncertain_pointcloud_filter_node started");

  std::string input_topic = argv[1];
  ROS_INFO("Input topic: %s",input_topic.c_str());
  std::string output_topic = argv[2];
  ROS_INFO("Output topic: %s",output_topic.c_str());
  std::string estimated_sensor_tf_frame = argv[3];
  ROS_INFO("Estimated Sensor TF frame: %s",estimated_sensor_tf_frame.c_str());
  std::string groundtruth_sensor_tf_frame = argv[4];
  ROS_INFO("Ground truth Sensor TF frame: %s",groundtruth_sensor_tf_frame.c_str());

  UncertainSensorReprojector reprojector(input_topic,
                                         output_topic,
                                         estimated_sensor_tf_frame,
                                         groundtruth_sensor_tf_frame);

 */
  while(ros::ok()) {
    ROS_INFO("Spinning");
    ros::spinOnce();
    ROS_INFO("Spinned");
  }

  return 0;
}
