#include <ros/ros.h>

#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

class PointCloudFilter {
public:
  typedef sensor_msgs::PointCloud2 PointcloudROS;
  typedef pcl::PointCloud<pcl::PointXYZ> PointcloudPCL ;

  PointCloudFilter();

  PointCloudFilter(std::string input_topic,
                   std::string output_topic);
  void filter(PointcloudPCL::Ptr noisy_pointcloud,
              PointcloudPCL::Ptr filtered_pointcloud);
  void setInputTopic(std::string topic);
  void setOutputTopic(std::string topic);


  virtual void pointcloudCallback(const PointcloudROS::ConstPtr& pointcloud);
protected:
  ros::NodeHandle nh_;
  ros::Subscriber pointcloud_sub_;
  ros::Publisher pointcloud_pub_;
};

PointCloudFilter::PointCloudFilter() {}

PointCloudFilter::PointCloudFilter(const std::string input_topic,
                                   const std::string output_topic) {
  nh_ = ros::NodeHandle();
  setInputTopic(input_topic);
  setOutputTopic(output_topic);
}

void PointCloudFilter::filter(const PointcloudPCL::Ptr noisy_pointcloud,
                              PointcloudPCL::Ptr filtered_pointcloud) {
  ROS_WARN("Filter functionallity is not implemented");
}


void PointCloudFilter::setInputTopic(const std::string topic) {
  pointcloud_sub_ =  nh_.subscribe(topic, 1,
                                   &PointCloudFilter::pointcloudCallback, this);
}

void PointCloudFilter::setOutputTopic(const std::string topic) {
  pointcloud_pub_ =  nh_.advertise<PointcloudROS>(topic, 1);
}

void PointCloudFilter::pointcloudCallback(
    const PointcloudROS::ConstPtr &pointcloud) {
  static PointcloudPCL::Ptr filtered_cloud(new PointcloudPCL);
  static PointcloudPCL::Ptr noisy_cloud(new PointcloudPCL);
  static PointcloudROS cloud_msg;

  pcl::fromROSMsg(*pointcloud, *noisy_cloud);
  filter(noisy_cloud, filtered_cloud);
  pcl::toROSMsg(*filtered_cloud,cloud_msg);

  pointcloud_pub_.publish(cloud_msg);
}






class UncertainSensorReprojector : PointCloudFilter {
public:
  UncertainSensorReprojector();
  UncertainSensorReprojector(std::string input_topic,
                             std::string output_topic,
                             std::string uncertain_sensor_frame,
                             std::string groundtruth_sensor_frame);
  void setGrountruthSensorTfFrame(std::string tf_frame);
  void setUncertainSensorTfFrame(std::string tf_frame);

  void transformPointcloudToGroundtruthSensor(
      PointcloudPCL::ConstPtr uncertain_pointcloud,
      PointcloudPCL::Ptr groundtruth_pointcloud);

  void pointcloudCallback(const PointcloudROS::ConstPtr& pointcloud) override;
private:
  std::string groundtruth_sensor_frame_;
  std::string uncertain_sensor_frame_;
};

UncertainSensorReprojector::UncertainSensorReprojector() {}

UncertainSensorReprojector::UncertainSensorReprojector(
    const std::string input_topic,
    const std::string output_topic,
    const std::string uncertain_sensor_frame,
    const std::string groundtruth_sensor_frame) :
  PointCloudFilter(input_topic,output_topic)
{
  setGrountruthSensorTfFrame(groundtruth_sensor_frame);
  setUncertainSensorTfFrame(uncertain_sensor_frame);
}

void UncertainSensorReprojector::setGrountruthSensorTfFrame(
    const std::string tf_frame) {
  groundtruth_sensor_frame_ = tf_frame;
}

void UncertainSensorReprojector::setUncertainSensorTfFrame(
    const std::string tf_frame) {
  uncertain_sensor_frame_ = tf_frame;
}

void UncertainSensorReprojector::transformPointcloudToGroundtruthSensor(
    const PointcloudPCL::ConstPtr uncertain_pointcloud,
    PointcloudPCL::Ptr groundtruth_pointcloud) {
  static tf::TransformListener tf_listener;
  static ros::Time time_to_lookup = (ros::Time) uncertain_pointcloud->header.stamp;

  // Convert pointcloud to sensor frame (e.g. if it is in "/world" frame)
  if (uncertain_sensor_frame_ != uncertain_pointcloud->header.frame_id) {

    if (!tf_listener.canTransform(uncertain_sensor_frame_,
                                  uncertain_pointcloud->header.frame_id,
                                  time_to_lookup)) {
      time_to_lookup = ros::Time(0);
      ROS_WARN("Using latest TF transform instead of timestamp match.");
    }

    tf::StampedTransform tf_transform_stamped;
    try {
      tf_listener.lookupTransform(uncertain_sensor_frame_,
                                  uncertain_pointcloud->header.frame_id,
                                  time_to_lookup,
                                  tf_transform_stamped);
    } catch (tf::TransformException& ex) {
      ROS_ERROR_STREAM(
            "Error getting TF transform from sensor data: " << ex.what());
      return;
    }

    Eigen::Affine3d affine_tranf;
    tf::transformTFToEigen(tf::Transform(tf_transform_stamped.getRotation(),
                                         tf_transform_stamped.getOrigin()),
                           affine_tranf);

    pcl::transformPointCloud(*uncertain_pointcloud,
                             *groundtruth_pointcloud,
                             affine_tranf.cast<float>());
  } else {
    *groundtruth_pointcloud = *uncertain_pointcloud;
  }
  //groundtruth_pointcloud->header.stamp = ros::Time::now().toNSec()/1e3;
  groundtruth_pointcloud->header.frame_id = groundtruth_sensor_frame_;
}


void UncertainSensorReprojector::pointcloudCallback(
    const PointcloudROS::ConstPtr &pointcloud) {
  PointcloudPCL::Ptr uncertain_cloud(new PointcloudPCL);
  PointcloudPCL::Ptr groundtruth_cloud(new PointcloudPCL);
  PointcloudROS cloud_msg;

  pcl::fromROSMsg(*pointcloud, *uncertain_cloud);
  transformPointcloudToGroundtruthSensor(uncertain_cloud, groundtruth_cloud);
  pcl::toROSMsg(*groundtruth_cloud,cloud_msg);

  pointcloud_pub_.publish(cloud_msg);
}






int main(int argc, char** argv)
{
  ros::init(argc, argv, "Uncertain_pointcloud_filter_node");

  if (argc!=5) {
    ROS_FATAL("Incorrect number of arguments.\n\
usage: uncertain_pointcloud_filter_node \
<input_topic> <output_topic> \
<uncertain_sensor_tf_frame> <groundtruth_sensor_tf_frame>");
    return -1;
  }

  ROS_INFO("Uncertain_pointcloud_filter_node started");

  std::string input_topic = argv[1];
  ROS_INFO("Input topic: %s",input_topic.c_str());
  std::string output_topic = argv[2];
  ROS_INFO("Output topic: %s",output_topic.c_str());
  std::string estimated_sensor_tf_frame = argv[3];
  ROS_INFO("Estimated Sensor TF frame: %s",estimated_sensor_tf_frame.c_str());
  std::string groundtruth_sensor_tf_frame = argv[4];
  ROS_INFO("Ground truth Sensor TF frame: %s",groundtruth_sensor_tf_frame.c_str());

  UncertainSensorReprojector reprojector(input_topic,
                                         output_topic,
                                         estimated_sensor_tf_frame,
                                         groundtruth_sensor_tf_frame);


  ros::spin();
  return 0;
}

